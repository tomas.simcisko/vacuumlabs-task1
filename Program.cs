﻿using System;


namespace ConsoleApplication
{
    public class Program
    {
        public static void Main(string[] args)
        {
            int boardSize = 0;
            int[ , ] board;

            Console.WriteLine("Enter board size: ");   

            try {
                boardSize = Convert.ToInt32(Console.ReadLine());
            }
            catch (Exception ex) {
                writeError("You must enter a number !!! ");
                return;
            }

            Console.WriteLine(boardSize);            

            if (boardSize < 1) {
                writeError("Size must be greater than 0 !!!");
            } 
            else {
                ChessBoard newBoard = new ChessBoard(boardSize);
                newBoard.start();
            }


        }
        
        public static int[ , ] initBoard(int size) {
            int[,] board = new int[size, size];

            for (int i = 0; i < board.GetLength(0); i++) {
                for(int j = 0; j < board.GetLength(1); j++) {
                    board[i, j] = 0;
                }
            }

            return board;
        }

        public static void printBoard(int[ , ] board) {
            for (int i = 0; i < board.GetLength(0); i++) {
                for(int j = 0; j < board.GetLength(1); j++) {               
                    Console.Write(board[i, j] + " ");                    
                }
                Console.WriteLine();
            }
        }
        
        public static void writeError(string str) {
            Console.ForegroundColor = ConsoleColor.Red;
            Console.WriteLine(str);   
            Console.ResetColor();
        }

        public static void placeQueen(int[,] board) {
             for (int i = 0; i < board.GetLength(0); i++) {
                for(int j = 0; j < board.GetLength(1); j++) {
                    if (canBePlaced(i, j, board)) {
                        board[i, j] = 1;
                        Console.WriteLine("queen placed");
                    }
                }
            }
        }

        //returs true, if there is no Queen in given row and column or diagonaô
        public static bool canBePlaced(int column, int row, int[,] board) {
            bool canPlace = false;
             for (int i = 0; i < board.GetLength(0); i++) {
                for(int j = 0; j < board.GetLength(1); j++) {
                    if(board[i, column] == 0 || board[row, j] == 0 ) {
                        canPlace = true;
                    }
                    canPlace = false;
                }
            }
            return canPlace;            
        }

    }
}
