﻿using System;

namespace ConsoleApplication
{
    public class ChessBoard
    {
        private static int[ , ] board;
        private int numQueens;
        private static int size;

        public ChessBoard(int sizeParam) {
            numQueens = 0;
            Console.WriteLine("size" + size);
            size  = sizeParam;
            board = new int[size, size];

            for(int i = 0; i < size; i++) {
                for(int j = 0; j < size; j++) {
                    board[i, j] = 0;
                }
            }
        }

        public int getNumQueens() {
            return 0;
        }

        public void start() {
            solve(0);
        }

        public bool solve(int numQueens) {
            if(numQueens == size) {
                Console.WriteLine("DONE");
                printBoard();

                return true;
            }
            else {
                for(int i = 0; i < size; i++) {
                    for(int j = 0; j < size; j++) {
                        if(validMove(i, j) == 0) {
                            placeQueen(i, j, 0);
                            numQueens++;
                            if(solve(numQueens)) {
                                return true;
                            }
                            else {
                                placeQueen(i, j, 1);
                                numQueens--;
                            }
                        }
                    }
                }
            }
            
            return false;
        }

        public static int validMove(int x, int y) {
             for(int i = 0; i < size; i++) {
                if(get(x, i) == 1) {
                    return -1;
                }
                else if(get(i, y) == 1) {
                    return -1;
                }
            }

            //diagonals

            for(int i = 0; i < size; i++) {
                if(get(x - i, y - i) == 1) {
                    return -1;
                }
                if(get(x - i, y + i) == 1) {
                    return -1;
                }
                if(get(x + i, y - i) == 1) {
                    return -1;
                }
                if(get(x + i, y + i) == 1) {
                    return -1;
                }
            }
            
            return 0;
        }

        public int placeQueen(int x, int y, int type) {
            if(type == 0) {
                board[x, y] = 1;
                numQueens++;
                
                return 0;
            }
            else if(type == 1) {
                board[x, y] = 0;
                return 0;
            }
            
            Console.WriteLine("Wrong type");
            return -3;
        }

        public static int get(int x, int y) {
            
            if(x < 0 || y < 0 || x > size - 1 || y > size - 1) {
                Console.WriteLine("Get error");

                return -1;
            }
            
            return board[x, y];
        }

        public void printBoard() {
            for(int i = 0; i < size; i++) {
                for(int j = 0; j < size; j++) {
                    Console.Write(board[i, j] + " ");
                }
                Console.WriteLine("");
            }            
        }

    }
}
